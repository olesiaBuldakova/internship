import UIKit

import UIKit

let myName = "Olesia"
var myInt = 8
myInt = 16
let myDouble = 2.189
typealias fechedResult = [String: Any]
let isActiveState = true
if isActiveState {
    print("you can start working")
} else {
    print("you are to activate tool")
}

let myCortege = (someNumber: 3, someText: "this is an example of cortege")
print("the number of my cortege is \(myCortege.someNumber), the text of my cortege is \(myCortege.1)")
var someNumber: Int?
someNumber = 7
if someNumber != nil {
    print("value of number = \(someNumber!)")
}
if let currentNumber = someNumber {
    print(currentNumber)
}

let myAge = 20
assert(myAge >= 18)
if myAge > 18 {
    print("you can enjoy time spending at a bar")
} else {
    assertionFailure("you are not allowed to go to bar")
}
//precondition(myAge > 21, "age when print is impossible")
precondition(myAge >= 20, "age when print is possible")
print(myAge)

let firstInt = 3
var secontInt = 6
secontInt = firstInt
print(secontInt)

let sum = firstInt + secontInt
let subtraction = firstInt - secontInt
let multiplication = firstInt * secontInt
let devision = firstInt / secontInt

let mySurname = "Buldakova"
let name = myName + " " + mySurname

secontInt += 3
secontInt % 2

(1, "towel") < (1, "bird")
(2, "flower") > (2, "zombie")
secontInt > firstInt
secontInt == 6
firstInt <= 3
secontInt != 3

var isGreater4 = true
let result = isGreater4 ? secontInt : firstInt
isGreater4 = false
let anotherResult = isGreater4 ? secontInt : firstInt

var newFriend: String?
let bestFriend = "Olesia"
var currentFriend = newFriend ?? bestFriend
newFriend = "Veronica"
currentFriend = newFriend ?? bestFriend

let arrayInt = [1, 2, 3, 4, 5, 6]
let arrayCount = arrayInt.count
for i in 0...arrayCount - 1 {
    print(arrayInt[i])
}
for i in 0..<arrayCount {
    print(arrayInt[i])
}
for number in arrayInt[3...] {
    print(number)
}
for number in arrayInt[..<3] {
    print(number)
}

let isOlder18 = true
let isHaveDriveLisence = true
let isHaveCar = false
if isOlder18 && isHaveDriveLisence {
    print("you're allowed to drive")
} else {
    print("sorry, you don't have a permission")
}

if isOlder18 && isHaveDriveLisence && isHaveCar {
    print("congratulations!")
} else {
    print("sorry")
}
if (isOlder18 && isHaveDriveLisence) || isHaveCar {
    print("you're allowed to drive")
} else {
    print("sorry, you don't have a permission")
}



