import UIKit

struct TestScript {
    let someValue: Int
    var stringValue: String
    subscript(value1: String) -> String {
        get {
          return stringValue + value1
        }
        set {
           return stringValue = "someValue"
        }
    }
    subscript(value: Int) -> Int {
        return someValue + value // если get only, то можно опускать как и в вычисляемых свойствах
    }
}

var newValue = TestScript(someValue: 5, stringValue: "")
print(newValue[5])
newValue = TestScript(someValue: 2, stringValue: "name")
print(newValue["eeee"])
