import UIKit

class Car {
    var owner = ""
    init (womanOwner: String) {
        owner = womanOwner
    }
    init (manOwner: String) {
        owner = manOwner
    }
}
let womanOwner = Car(womanOwner: "Katya")
print(womanOwner.owner)
let manOwner = Car(manOwner: "Tolya")
print(manOwner.owner)

enum TemperatureUnit {
    case kelvin, celsius, fahrenheit
    init?(symbol: Character) {
        switch symbol {
        case "K":
            self = .kelvin
        case "C":
            self = .celsius
        case "F":
            self = .fahrenheit
        default:
            return nil
        }
    }
}

let fahrenheitUnit = TemperatureUnit(symbol: "F")
if fahrenheitUnit != nil {
    print("Эта единица измерения температура определена, а значит наша инициализация прошла успешно!")
}

let unknownUnit = TemperatureUnit(symbol: "X")
if  unknownUnit == nil {
    print("Единица измерения температуры не определена, таким образом мы зафейлили инициализацию")
}
