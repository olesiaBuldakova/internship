import UIKit

struct SomeProperties {
    var firstValue: Int
    let count: Int
}

var first4Values = SomeProperties(firstValue: 1, count: 4)
first4Values.firstValue = 5
print(first4Values)
let someValue = SomeProperties(firstValue: 3, count: 3)
// someValue.firstValue = 5 так нельзя, тк someValue let константа, те мы ее не можем изменить несмотря на то, что firstValue это var


