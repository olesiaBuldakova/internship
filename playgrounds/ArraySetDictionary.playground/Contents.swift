import UIKit

var names = ["olya", "sveta", "ivan", "misha", "tom"]
names.count
if names.isEmpty {
    print("empty")
} else {
    print("have elements")
}
names.append("sasha")
names += ["oleg"]
print(names)
names.remove(at: 5)
names[0]
names.removeFirst()
for (index, value) in names.enumerated() {
    print("\(index + 1) = \(value)")
}

var setString: Set = ["olya", "sveta", "ivan"]
setString.count
if setString.isEmpty {
    print("empty")
} else {
    print("have elements")
}
setString.insert("tanya")
setString.removeFirst()
setString.remove("ivan")
if setString.contains("olya") {
    print("hi olya")
} else {
    print("try one more time")
}
setString.sorted()
for name in setString {
    print(name)
}
print(setString)
var anotherSetString: Set = ["morning", "evening", "afternoon", "tanya", "sveta"]
let newSet = setString.intersection(anotherSetString)
print(newSet)
let symmetricSet = setString.symmetricDifference(anotherSetString)
print(symmetricSet)
let unionSet = setString.union(anotherSetString)
print(unionSet)
let substractingSet = setString.subtracting(anotherSetString)
print(substractingSet)
let strictSubSet = setString.isStrictSubset(of: anotherSetString)
print(strictSubSet)
let strictSuperSet = setString.isStrictSuperset(of: anotherSetString)
print(strictSuperSet)

var someDictionary = [Int: String]()
var currencies = ["$": "dollar", "rub": "ruble", "byn": "bel ruble"]
currencies["euro"] = "euro"
currencies.updateValue("bellorusian ruble", forKey: "byn")
print(currencies["byn"])
currencies["byn"] = nil
currencies.removeValue(forKey: "euro")
print(currencies)
for (key, value) in currencies {
    print("\(key) : \(value)")
}
for key in currencies.keys {
    print(key)
}
for value in currencies.values {
    names.append(value)
}
print(names)
currencies.keys.sorted()
let signs = [String](currencies.keys)
let currentCurrency = [String](currencies.values)

