import UIKit

let multipleLines = """

some text
anoter some text
and text one more time

"""

print(multipleLines)

let multipleLines1 = """

some text \
anoter some text \
and text one more time \

"""

print(multipleLines1)

let linesWithIndentation = """
строка без пробелов
строка с пробелами
строка без пробелов
"""
print(linesWithIndentation)
let wiseWords = "\"this is a quote\" - he said"
let dollarSign = "\u{24}"
let blackHeart = "\u{2665}"
let sparklingHeart = "\u{1F496}"

print(dollarSign,blackHeart,sparklingHeart)

var emptyString = ""
var anotherEmptyString = String()

if emptyString.isEmpty {
    print("empty")
}
if anotherEmptyString.isEmpty {
    print("empty")
}

let newString = "something"
for character in newString {
    print(character)
}
let someCharacters: [Character] = ["L", "o", "v","e", "!"]
let someString = String(someCharacters)
print(someString)

var string1 = "hi"
var string2 = "everybody"
string1 += string2
let newCharacter = "!"
string1 += newCharacter
string1.append(newCharacter)

let begining = """
first line
"""
let end = """

second line
third line
"""
print(begining + end)

let greeting = "Hello everybody!"
greeting[greeting.startIndex]
greeting[greeting.index(before: greeting.endIndex)]
greeting[greeting.index(after: greeting.startIndex)]
let index = greeting.index(greeting.startIndex, offsetBy: 6)
greeting[index]
var testWord = "something"
testWord.insert("!", at: testWord.endIndex)
testWord.insert(contentsOf:" there", at: testWord.index(before: testWord.endIndex))
testWord.remove(at: testWord.index(after: testWord.startIndex))
let firstLine = "this is first line"
let anotherLine = "this is first line"
firstLine == anotherLine
var threeInt = Array(repeating: 2, count: 3)
var anotherThreeInt = Array(repeating: 1, count: 2)
print(threeInt + anotherThreeInt)

