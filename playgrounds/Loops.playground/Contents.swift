import UIKit

let interval = 2
for i in stride(from: 0, to: 10, by: interval) {
    print(i)
}
let limit = 10
for i in stride(from: 0, through: limit, by: interval) {
     print(i)
}
var start = 100
while start >= 0 {
    start -= 10
    if start == 0 {
        print("we have nothing")
    }
}

repeat {
    start += 10
} while start  <= 100

for i in 0...9 {
    start -= 10
    print(start)
    if start == 0 {
    print("empty")
    }
}

let grade = 1
switch grade {
case 1:
    print("it's your first year at school")
case 11:
 print("it's your first last at school")
 default:
    print("you'll spend some more time at school")
}
let someInt = 7
switch someInt {
case 1,3,5,7,9:
    print("it's an odd number")
case 2,4,6,8:
  print("it's an even number")
default:
    print("choose another number")
}
let somePoint = (0, 0)
switch somePoint {
case (-2...2, -2...2):
    print("\(somePoint) is inside the box")
case (_, 0):
    print("\(somePoint) is on the x-axis")
case (0, _):
    print("\(somePoint) is on the y-axis")
case (0, 0):
    print("\(somePoint) is at the origin")
default:
    print("\(somePoint) is outside of the box")
}
// somePoint = (0, 0) соответствует всем кейсам, но выведется первый,которому соответствует, а не более точный
let point = (1, -1)
switch point {
case let (x, y) where x == y:
    print("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    print("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    print("(\(x), \(y)) is just some arbitrary point")
}
let anotherPoint = (2, 2)
switch anotherPoint {
case let (x, y) where x == y:
    print("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    print("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    print("(\(x), \(y)) is just some arbitrary point")
}
let thirdPoint = (3, -1)
switch thirdPoint {
case let (x, y) where x == y:
    print("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    print("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    print("(\(x), \(y)) is just some arbitrary point")
}
let puzzleInput = "great minds think alike"
var puzzleOutput = ""
let charactersToRemove: [Character] = ["a", "e", "i", "o", "u", " "]
for character in puzzleInput {
    if charactersToRemove.contains(character) {
        continue // тут операция текущая заканчивается, и мы начинаем новую
    } else {
        puzzleOutput.append(character)
    }
}
print(puzzleOutput)
let newPoint = (0, 0)
var pointDescription = ""
switch newPoint {
case (-2...2, -2...2):
   pointDescription = "\(somePoint) is inside the box"
    fallthrough
case (_, 0):
   pointDescription += "\(somePoint) is on the x-axis"
    fallthrough
case (0, _):
    pointDescription += "\(somePoint) is on the y-axis"
    fallthrough
case (0, 0):
    pointDescription += "\(somePoint) is at the origin"
    fallthrough
default:
    pointDescription += "\(somePoint) is outside of the box"
}
print(pointDescription)  // соответствует всем , поэтому на печать выведет все случаи
