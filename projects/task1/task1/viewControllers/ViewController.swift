//
//  ViewController.swift
//  task1
//
//  Created by Леся Булдакова on 18/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let imageManager = CustomImageView()
    @IBOutlet weak var userPhotoImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageManager.makeCornersRounded(userPhotoImageView)
    }
}

