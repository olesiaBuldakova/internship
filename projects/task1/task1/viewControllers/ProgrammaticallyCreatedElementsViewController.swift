//
//  ProgrammaticallyCreatedElementsViewController.swift
//  task1
//
//  Created by Леся Булдакова on 18/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import UIKit
import SnapKit

class ProgrammaticallyCreatedElementsViewController: UIViewController {
    
        lazy var userNameLabel: UILabel = {
            let label = UILabel()
            label.text = "Олеся"
            label.textColor = .black
            label.font = UIFont.init(name: "Helvetica", size: 20)
            label.textAlignment = .center
            return label
        }()
    
        lazy var userSurnameLabel: UILabel = {
            let label = UILabel()
            label.text = "Булдакова"
            label.textColor = .black
            label.font = UIFont.init(name: "Helvetica", size: 20)
            label.textAlignment = .center
            return label
        }()
    
        lazy var userPhotoImageView: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "IMG_0392")
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            return imageView
        }()
    
        lazy var nameStackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [userNameLabel,userSurnameLabel])
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.spacing = 10
            return stackView
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            createImageViewConstraints ()
            createStackViewConstraints ()
        }
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            userPhotoImageView.layer.cornerRadius = userPhotoImageView.frame.height/2
        }
    
        func createImageViewConstraints () {
            view.addSubview(userPhotoImageView)
            userPhotoImageView.snp.makeConstraints { (make) -> Void in
                make.width.height.equalTo(200)
                make.center.equalTo(self.view)
            }
        }
        
        func createStackViewConstraints () {
            view.addSubview(nameStackView)
            nameStackView.snp.makeConstraints { (make) in
                make.width.equalTo(250)
                make.height.equalTo(50)
                make.top.equalTo(userPhotoImageView.snp.bottom).offset(20)
                make.centerX.equalTo(view)
            }
        }
}





