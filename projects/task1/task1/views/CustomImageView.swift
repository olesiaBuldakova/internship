//
//  CustomImage.swift
//  task1
//
//  Created by Леся Булдакова on 18/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation
import UIKit
class CustomImageView {
    func makeCornersRounded(_ image: UIImageView) {
        image.layer.cornerRadius = image.frame.height/2
    }
}

