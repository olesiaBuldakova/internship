//
//  Card.swift
//  concentration
//
//  Created by Леся Булдакова on 22/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation

struct Card {
    
    var isFaceUp = false
    var isMatch = false
    var identifier: Int
    static var identifierFactory = 0
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
}
