//
//  Concentration.swift
//  concentration
//
//  Created by Леся Булдакова on 22/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation

class Concentration {
    
    var cards = [Card]()
    var flipsCounter = 0
    var score = 0
    var selectedIndex = Set<Int>()
    var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            return   selectIndex()
        }
        set {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    init (numberOfPairsOfCard: Int) {
        for _ in 0..<numberOfPairsOfCard {
            let card = Card()
            let matchingCard = card
            cards += [card, matchingCard]
            cards.shuffle()
        }
    }
    
    func chooseCard(at index: Int) {
        let cardWasPreviouslyChosen = selectedIndex.contains(index)
        guard !cards[index].isMatch else { return }
        guard let matchIndex = indexOfOneAndOnlyFaceUpCard else {
            cards[index].isFaceUp = true
            indexOfOneAndOnlyFaceUpCard = index
            selectedIndex.insert(index)
            if cardWasPreviouslyChosen { score -= 1 }
            return
        }
        if matchIndex != index {
            if cards[matchIndex].identifier == cards[index].identifier {
                cards[matchIndex].isMatch = true
                cards[index].isMatch = true
                score += 2
            } else {
                if cardWasPreviouslyChosen { score -= 1 }
            }
        }
        cards[index].isFaceUp = true
        selectedIndex.insert(index)
    }
    
    func selectIndex() -> Int? {
        var foundIndex:Int?
        for index in cards.indices {
            if cards[index].isFaceUp {
                guard foundIndex == nil else { return nil }
                foundIndex = index
            }
        }
        return foundIndex
    }
    
    func resetGame() {
        for index in cards.indices {
            cards[index].isFaceUp = false
            cards[index].isMatch = false
        }
        indexOfOneAndOnlyFaceUpCard = nil
        selectedIndex.removeAll()
        score = 0
        flipsCounter = 0
    }
}
