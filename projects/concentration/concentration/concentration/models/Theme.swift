//
//  Theme.swift
//  concentration
//
//  Created by Леся Булдакова on 22/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation

class Theme {
    
    var allThemes = [Any]()
    let animalTheme = ["🙊","🦊","🐕","🐶","🐯","🐷","🐭","🐰","🐻","🐨","🐥","🦆"]
    let vegetableTheme = ["🥕","🥦","🍅","🌽","🥒","🌶","🍆","🥔","🥑","🥬","🥜","🌰"]
    let sportTheme = ["🚴","⛷️","🏇","🏂","🏋️‍♀️","⚽","🥊","🏊‍♂️","🤸","🛹","🏀","🏓"]
    let faceTheme = ["😂","😍","😇","🤠","😎","🤢","🥵","😈","🤡","💀","👺","🥶"]
    let fruitTheme = ["🍎","🥝","🍇","🍉","🍊","🍌","🍑","🍐","🍒","🍓","🍍","🍏"]
    let thigsTheme = ["🎈","⛱️","☎","📸","📌","🔑","🧲","🧯","🧺","📙","📟","🎁"]
    
    init () {
        allThemes.append(animalTheme)
        allThemes.append(vegetableTheme)
        allThemes.append(sportTheme)
        allThemes.append(thigsTheme)
        allThemes.append(fruitTheme)
        allThemes.append(faceTheme)
    }
    
    func chooseCurrentTheme() -> [String] {
        var choosenTheme = [String]()
        if let currentTheme = allThemes.randomElement() as? [String] {
            choosenTheme = currentTheme
        }
        return choosenTheme
    }
}
