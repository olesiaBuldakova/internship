//
//  ViewController.swift
//  concentration
//
//  Created by Леся Булдакова on 19/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var game = Concentration(numberOfPairsOfCard: (cardButtons.count + 1)/2)
    var emoji = [Int: String]()
    let themePicker = Theme()
    var emojiChoice = [String]()
    
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var flipsLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emojiChoice = themePicker.chooseCurrentTheme()
    }
    
    @IBAction func cardButtonClicked(_ sender: UIButton) {
        game.flipsCounter += 1
        guard let cardNumber = cardButtons.firstIndex(of: sender)  else {return }
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
        updateFlipsAndScore(flips: "Flips: \(game.flipsCounter)", score: "Score: \(game.score)")
    }
    
    @IBAction func newGameButtonClicked(_ sender: UIButton) {
        game.resetGame()
        updateFlipsAndScore(flips: "Flips: 0", score: "Score: 0")
        emojiChoice.removeAll()
        emoji.removeAll()
        emojiChoice = themePicker.chooseCurrentTheme()
        updateViewFromModel()
    }
    
    func updateFlipsAndScore (flips: String, score: String) {
        scoreLabel.text = score
        flipsLabel.text = flips
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: .normal)
                button.backgroundColor = .white
            } else {
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatch ? #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 0): #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            }
        }
    }
    
    func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoice.count > 0 {
            let randomindex = Int.random(in: 0..<emojiChoice.count)
            emoji[card.identifier] = self.emojiChoice.remove(at: randomindex)
        }
        return emoji[card.identifier] ?? "?"
    }
}
