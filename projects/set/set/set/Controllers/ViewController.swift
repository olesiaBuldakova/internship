//
//  ViewController.swift
//  set
//
//  Created by Леся Булдакова on 25/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var game = SetGame()
    var timer = Timer()
    var runTime = 60
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var dealThreeMoreCardsButton: UIButton!
    @IBOutlet var cardButtons: [CustomButton]!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showFirst12Cards()
        startTimer()
    }
    
    // MARK: - Actions
    @IBAction func cardIsChosen(_ sender: CustomButton) {
        guard let selectedCardIndex = cardButtons.firstIndex(of: sender) else { return }
        if selectedCardIndex < game.cardsInGame.count {
            game.chooseCardAtIndex(card: game.cardsInGame[selectedCardIndex], index: selectedCardIndex)
        }
        updateScoreLabel(with: "\(game.score)")
        setupTitle()
        showSelection()
    }
    
    @IBAction func addThreeCards(_ sender: UIButton) {
        game.addCards(for: 3)
        setupTitle()
        if game.cardsInGame.count >= 24 || game.cards.isEmpty {
            dealThreeMoreCardsButton.isEnabled = false
        }
    }
    
    @IBAction func newGamePressed(_ sender: UIButton) {
        for button in cardButtons {
            button.setAttributedTitle(nil, for: .normal)
            button.decelectedCard()
        }
        updateScoreLabel(with: "0")
        showFirst12Cards()
        resetTimer()
        dealThreeMoreCardsButton.isEnabled = true
    }
    
    // MARK: - Setup  button view
    func showSelection() {
        for index in game.cardsInGame.indices {
            let button = cardButtons[index]
            let card = game.cardsInGame[index]
            button.highlightCards(isSelectButton: game.cardWasSelected(card: card), isSet: game.isCardsSet())
        }
    }
    
    func setupTitle() {
        for index in game.cardsInGame.indices {
            let button = cardButtons[index]
            let card = game.cardsInGame[index]
            let title = button.drowCard(cards: card)
            button.setAttributedTitle(title, for: .normal)
            button.backgroundColor = .white
        }
    }
    
    func showFirst12Cards() {
        game.newGame()
        setupTitle()
    }
    
    func updateScoreLabel(with value: String) {
        scoreLabel.text = "Score: \(value)"
    }
    
    // MARK: - Setup timer
    @objc func timerProcess() {
        if runTime > 0 {
            runTime -= 1
            timerLabel.text = "\(runTime)"
        } else if runTime == 0 {
            resetTimer()
            game.score -= 1
            updateScoreLabel(with: "\(game.score)")
        }
    }
    
    func resetTimer() {
        timer.invalidate()
        runTime = 60
        startTimer()
        timerLabel.text = "\(runTime)"
    }
    
    func startTimer () {
        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(self.timerProcess),
            userInfo: nil,
            repeats: true)
        timerLabel.text = "\(runTime)"
    }
}

