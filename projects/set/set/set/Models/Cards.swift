//
//  Cards.swift
//  set
//
//  Created by Леся Булдакова on 25/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation

enum Color: CaseIterable {
    case red, green, blue
}

enum Shape: String, CaseIterable {
    case circle = "●"
    case square = "■"
    case triangle = "▲"
}

enum NumberOfFigures: CaseIterable {
    case one, two, three
}

enum Shading: CaseIterable {
    case outline, filled, stride
}

struct Cards: Hashable {
    
    var color: Color
    var symbol: Shape
    var numberOfFigures: NumberOfFigures
    var shading: Shading
    var identifier: Int
    static var identifierFactory = 0
    
    init(color: Color, shape: Shape, numberOfFigures: NumberOfFigures, shading: Shading) {
        self.color = color
        self.numberOfFigures = numberOfFigures
        self.symbol = shape
        self.shading = shading
        self.identifier = Cards.getUniqueIdentifier()
    }
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    static func == (lhs: Cards, rhs: Cards) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(color)
        hasher.combine(symbol)
        hasher.combine(numberOfFigures)
        hasher.combine(shading)
        hasher.combine(identifier)
    }
}
