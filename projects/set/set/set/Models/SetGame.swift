//
//  SetGame.swift
//  set
//
//  Created by Леся Булдакова on 25/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation

class SetGame {
    var cards = [Cards]()
    var cardsInGame = [Cards]()
    var selectedCards = [Cards]()
    var isSet = false
    let startNumberOfcards = 12
    var indexiesOfSelectedCards = Set<Int>()
    var isCardWasPreviouslyChosen = false
    var score = 0
    
    init () {
        generateCards()
    }
    
    func generateCards() {
        for color in Color.allCases {
            for shape in Shape.allCases {
                for number in NumberOfFigures.allCases {
                    for shading in Shading.allCases {
                        let card = Cards(color: color, shape: shape, numberOfFigures: number, shading: shading)
                        cards.append(card)
                    }
                }
            }
        }
        cards.shuffle()
    }
    
    func addCard() {
        let selectedCard = cards.remove(at: cards.count.random())
        cardsInGame.append(selectedCard)
    }
    
    func addCards(for numberOfCards: Int)  {
        for _ in 0..<numberOfCards {
            addCard()
        }
    }
    
    func newGame() {
        cards.removeAll()
        cardsInGame.removeAll()
        selectedCards.removeAll()
        generateCards()
        score = 0
        addCards(for: startNumberOfcards)
    }
    
    func cardWasSelected(card: Cards) -> Bool {
        return selectedCards.firstIndex(of: card) != nil
    }
    
    func isMatchingColor(card1: Cards, card2: Cards, card3: Cards) -> Bool {
        if card1.color == card2.color && card2.color == card3.color {
            return true
        } else if card1.color != card2.color && card2.color != card3.color, card1.color != card3.color {
            return true
        } else {
            return false
        }
    }
    
    func isMatchingSymbols(card1: Cards, card2: Cards, card3: Cards) -> Bool {
        if card1.symbol == card2.symbol && card2.symbol == card3.symbol {
            return true
        } else if card1.symbol != card2.symbol && card2.symbol != card3.symbol, card1.symbol != card3.symbol {
            return true
        } else {
            return false
        }
    }
    
    func isMatchingNumberOfSymbols(card1: Cards, card2: Cards, card3: Cards) -> Bool {
        if card1.numberOfFigures == card2.numberOfFigures && card2.numberOfFigures == card3.numberOfFigures {
            return true
        } else if card1.numberOfFigures != card2.numberOfFigures && card2.numberOfFigures != card3.numberOfFigures && card1.numberOfFigures != card3.numberOfFigures {
            return true
        } else {
            return false
        }
    }
    
    func isMatchingShading(card1: Cards, card2: Cards, card3: Cards) -> Bool {
        if card1.shading == card2.shading && card2.shading == card3.shading {
            return true
        } else if card1.shading != card2.shading && card2.shading != card3.shading && card1.shading != card3.shading {
            return true
        } else {
            return false
        }
    }

    func isCardsSet() -> Bool{
        guard selectedCards.count != 3  else {
            let card1 = selectedCards[0]
            let card2 = selectedCards[1]
            let card3 = selectedCards[2]
            isSet = isMatchingColor(card1: card1, card2: card2, card3: card3)
                && isMatchingShading(card1: card1, card2: card2, card3: card3)
                && isMatchingNumberOfSymbols(card1: card1, card2: card2, card3: card3)
                && isMatchingSymbols(card1: card1, card2: card2, card3: card3)
        return isSet
        }
        isSet = false
        return isSet
    }
    
    func replaceCardsFromSet() {
        for card in selectedCards {
            if let selectedIndex = cardsInGame.firstIndex(of: card) {
                cardsInGame.remove(at: selectedIndex)
                guard  cards.count > 0  else { return }
                let chosenCard = cards.remove(at: cards.count.random())
                cardsInGame.insert(chosenCard, at: selectedIndex)
            }
        }
    }
    
    func updateScoreWithValue(value: Int) {
        score += value
        selectedCards.removeAll()
    }
    
    func chooseCardAtIndex(card: Cards, index:Int) {
        isCardWasPreviouslyChosen = indexiesOfSelectedCards.contains(index)
        if selectedCards.count == 3 && isCardsSet() {
            replaceCardsFromSet()
            updateScoreWithValue(value: 3)
            indexiesOfSelectedCards.removeAll()
        } else if selectedCards.count == 3 && !isCardsSet() {
            indexiesOfSelectedCards.removeAll()
            updateScoreWithValue(value: -1)
        }
        if isCardWasPreviouslyChosen {
            guard let indexToRemove = selectedCards.firstIndex(of: card) else { return }
            selectedCards.remove(at: indexToRemove)
        } else {
            selectedCards.append(card)
            indexiesOfSelectedCards.insert(index)
        }
    }
}
