//
//  Extensions.swift
//  set
//
//  Created by Леся Булдакова on 12/05/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func highlightSelectedCard(){
        self.layer.borderWidth = 3
        self.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    func decelectedCard() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func highlightSet() {
        self.layer.borderWidth = 3
        self.layer.borderColor = UIColor.green.cgColor
    }
}
extension Int {
    func random() -> Int {
        let randomValue = Int.random(in: 0..<self)
        return self > 0 ? randomValue : (self < 0 ? -randomValue : 0)
    }
}
