//
//  CustomButton.swift
//  set
//
//  Created by Леся Булдакова on 26/04/2019.
//  Copyright © 2019 Леся Булдакова. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    func getColor(card: Cards) -> UIColor {
        switch card.color {
        case .blue:
            return  .blue
        case .green:
            return  .green
        case .red:
            return  .red
        }
    }
    
    func getShymbol(card: Cards) -> String {
        switch card.symbol {
        case .circle:
            return Shape.circle.rawValue
        case .square:
            return Shape.square.rawValue
        case .triangle:
            return Shape.triangle.rawValue
        }
    }
    
    func getNumberOfFigures(card: Cards, symbol: String) -> String {
        switch card.numberOfFigures {
        case .one:
            return symbol
        case .two:
            return  String(repeating: symbol, count: 2)
            
        case .three:
            return String(repeating: symbol, count: 3)
        }
    }
    
    func getShade(card: Cards, color: UIColor, symbol: String)  -> NSAttributedString {
        var attributes: [NSAttributedString.Key: Any] = [:]
        switch card.shading {
        case .outline:
            attributes[.strokeWidth] = 3
            attributes[.foregroundColor] = color
        case .filled:
            attributes[.strokeWidth] = -1
            attributes[.foregroundColor] = color
        case .stride:
            attributes[.strokeWidth] = -1
            attributes[.foregroundColor] = color.withAlphaComponent(0.2)
        }
        return NSAttributedString(string: symbol, attributes: attributes)
    }
    
    func drowCard(cards: Cards) -> NSAttributedString {
        let color = getColor(card: cards)
        let symbol = getShymbol(card: cards)
        let symbolsOnCard = getNumberOfFigures(card: cards, symbol: symbol)
        let title = getShade(card: cards, color: color, symbol: symbolsOnCard)
        return title
    }
    
    func highlightCards(isSelectButton: Bool, isSet: Bool) {
        if isSelectButton {
            guard  isSet  else {
                highlightSelectedCard()
                return
            }
            highlightSet()
        } else {
            decelectedCard()
        }
    }
}
